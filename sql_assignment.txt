SQL TASK 10/01/2020  PRACHI KODLE

 1. List of consultants having more than one submission.
With resultset as (select consultant.c_name, count(consultant.c_id) as "number_of_submissions" from submission inner join consultant on consultant.c_id=submission.consultant_id group by c_name) select * from resultset where number_of_submissions > 1;
 c_name  | number_of_submissions
---------+-----------------------
 pallavi |                     3
 purva   |                     3
 anshul  |                     3
(3 rows)


2. List all interviews of consultants.
With resultset as (select i.int_id, s.consultant_id, i.round from interview i inner join submission s on i.sub_id=s.sub_id) Select r.int_id, c.c_id, c.c_name, r.round from resultset r inner join consultant c on r.consultant_id=c.c_id;
 int_id | c_id | c_name  | round
--------+------+---------+-------
      1 |    1 | pallavi |     1
      2 |    1 | pallavi |     1
      3 |   12 | anshul  |     1
      4 |    8 | purva   |     1
      5 |    1 | pallavi |     2
      6 |    1 | pallavi |     3
      7 |    1 | pallavi |     2
      8 |    8 | purva   |     2
      9 |    8 | purva   |     3
(9 rows)


3. List all PO of marketers.
select p.sub_id, start as joining_date, end as end_date from purchase_offer p ,(select sub_id from submission where empolyee_id in
(select emp_id from employee_role where role_id in (select role_id from role where role_name='Marketer'))) s where p.sub_id=s.sub_id;
 sub_id | joining_date |  end_date
--------+--------------+------------
      2 | 2020-01-01   | 2020-08-30
      3 | 2020-03-01   | 2020-10-30
     10 | 2020-02-01   | 2020-06-30
(3 rows)


4. Unique vendor company name for which client location is the same.
select distinct client.name as company_name from vendor inner join (select s1.vendor_id from submission s1,submission s2 where s1.city=s2.city and 
s2.sub_id!=s1.sub_id and s1.vendor_id!=s2.vendor_id) submission on vendor.vendor_id=submission.vendor_id; 
 company_name     
------------------
 Flipkart
 Paypal
 Amazon
 Google
 Facebook
(5 rows)


5. Count of consultants which are submitted in the same city.
select city, count(*) from submission group by city;
   city    | count
-----------+-------
 bangalore |     2
 pune      |     9
(2 rows)


6. Name of consultant and client who have been submitted on the same vendor.
 select c.c_name, cl.name, s.vendor_id from consultant c, client cl, submission s where s.client_id=cl.client_id and s.consultant_id=consultant_id;
  c_name  |   name   | vendor_id
----------+----------+-----------
 panshul  | Amazon   |         5
 paridhi  | Amazon   |         5
 pragya   | Amazon   |         5
 prakhar  | Amazon   |         5
 pritish  | Amazon   |         5
 pranay   | Amazon   |         5
 purva    | Amazon   |         5
 prashika | Amazon   |         5
 prateek  | Amazon   |         5
 anamika  | Amazon   |         5
 anshul   | Amazon   |         5
 yash     | Amazon   |         5
 nakul    | Amazon   |         5
 bhumika  | Amazon   |         5
 mansi    | Amazon   |         5
 muskan   | Amazon   |         5
 pallavi  | Amazon   |         5
 panshul  | Flipkart |         3
 paridhi  | Flipkart |         3
 pragya   | Flipkart |         3
 prakhar  | Flipkart |         3
 pritish  | Flipkart |         3
 pranay   | Flipkart |         3
 purva    | Flipkart |         3
 prashika | Flipkart |         3
 prateek  | Flipkart |         3
-- More  --






